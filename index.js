// ==UserScript==
// @name         Name
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  A script to add elements on Wize map
// @author       romain-dehasseleer,tunisiano187 
// @match        https://tampermonkey.net/scripts.php
// @grant        none
// @require      http://openlayers.org/api/OpenLayers.js
// @require      https://code.jquery.com/jquery-3.3.1.min.js
// @include      https://www.waze.com/*/editor/*
// @include      https://www.waze.com/editor/*
// @include      https://editor-beta.waze.com/*
// @exclude      https://www.waze.com/user/*editor/*

// ==/UserScript==

(function() {
    'use strict';
    // récuperer les données depuis Walonmap (avec ou sans api)
    // éventuellement inclure jquery dans l'entête pour la requête json
    function getPlaces(){

    }
    //dessiner le filtre  sur lequel placer les éléments

    function drawFilter(){
        // div contenant la map sur laquelle on doit dessiner le filtre
        //id OpenLayers_Layer_Vector_RootContainer_310_svgRoot


    }
    // place les éléments récupérer depuis Walonmap
    function placeItems(){

    }
    //Exemple d'URL avec Waze, où la lon et la lat sont les variables navigateurs
    //Le zoom est réglé sur 5 (défaut)
    //https://www.waze.com/editor?lon=3.95757&lat=50.45686&zoom=5

    //On récupère la position de l'utilisateur
    var latitude, longitude;

    //prévoir le cas où les données sont null
    navigator.geolocation.getCurrentPosition(function(location) {
        latitude = location.coords.latitude;
        longitude = location.coords.longitude;
        //pour l'instant on redirige vers Waze, dans le futur on effectue le traitement que si le domaine correspondant à waze.com/editor
        $(document).ready(function(){
            //la récupération doit être faite avant d'aller sue Waze à cause de la protection Cross-domain
            //changer l'url par ce qu'il faut
            var places;
            $.getJSON( "https://geoservices.wallonie.be/cadmap/rest/getListeCommunes", function( data ) {

                console.log(JSON.stringify(data));
                places=JSON.stringify(data);
                //place les items dans une liste à puce, temporaire
                /*
                var items = [];
                $.each( data, function( key, val ) {items.push( "<li id='" + key + "'>" + val + "</li>" );});

                $( "<ul/>", {"class": "my-new-list",html: items.join( "" )}).appendTo( "body" );
                console.log(items);
              */
            });
            alert('Hello world !');
            //window.location.href = "https://www.waze.com/editor?lon="+longitude+"&lat="+latitude+"&zoom=5";
            console.log(places); //vérification après chargement des données /!\ problème d'encodage détecté
            //todo :
            // /!\ ne pas oublier de vérifier chaque éléments de Wize(connecté, lvl non null,...)
            //dessiner le calque sur la map
            //récupérer le lvl de l'utilisateur et restreindre la liste des objets à ceux dans la zone (lvl 1 = 1 miles,etc)
            //place les items à leur coordonnées respectives+système d'édition des items onclick

        });

    });

})();